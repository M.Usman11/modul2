const golder = () => {
    console.log("this is golden!!");
}

golder();

console.log('/n');

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName() {
            console.log('${firstName} ${lastName}');
        }
    }
}

newFunction("William", "Imoh").fullName();

console.log('/n');

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, last, destination, occupation);

console.log('/n');

const west = ["will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log(combined);

console.log('/n');

const planet = "earth";
const view = "glass";

var before = `Lorem ${view} dolor sit amet, 
consectetur adipiscing elit, ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam`;

console.log(before);
